package com.NovumScientiaTeam.modulartoolkit.parts.partTypes.modifications;

public abstract class PartModification {
    public abstract double getExtraDurabilityMultiplier();
}
