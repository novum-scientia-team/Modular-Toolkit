package com.NovumScientiaTeam.modulartoolkit.parts.partTypes.modifications;

public class ToughMod extends PartModification {
    @Override
    public double getExtraDurabilityMultiplier() {
        return 2;
    }
}
